test("Valid Plates",function(assert){
    assert.equal(isValidPlate("1234BCD"), true, "1234 BCD is an invalid plate");
    assert.equal(isValidPlate("1223BCC"), true, "1223 BCC is an invalid plate");
    assert.equal(isValidPlate("1222BBB"), true, "1222 BBB is an invalid plate");
    assert.equal(isValidPlate("2222BBC"), true, "2222 BBC is an invalid plate");
});

test("Not Valid Plates (wrong number part)",function(assert){
    assert.equal(isValidPlate("12R4BCD"), false, "12R4 BCD is an invalid plate");
    assert.equal(isValidPlate("RTFRBCD"), false, "RTFR BCD is an invalid plate");
    assert.equal(isValidPlate("R234BCD"), false, "R234 BCD is an invalid plate");
    assert.equal(isValidPlate("12RDBCD"), false, "12RD BCD is an invalid plate");
    assert.equal(isValidPlate("12345BCD"), false, "12345 BCD is an invalid plate");
    assert.equal(isValidPlate("123BCD"), false, "123 BCD is an invalid plate");
});

//The one that has 5 numbers gives us as valid but is invalid.

test("Not Valid Plates (wrong letters part)",function(assert){
    assert.equal(isValidPlate("1234B2D"), false, "1234 B2D is an invalid plate");
    assert.equal(isValidPlate("1223B32"), false, "1223 B32 is an invalid plate");
    assert.equal(isValidPlate("1222252"), false, "1222 252 is an invalid plate");
    assert.equal(isValidPlate("2222AQI"), false, "2222 AQI is an invalid plate");
    assert.equal(isValidPlate("1234BÑD"), false, "1234 BÑD is an invalid plate");
    assert.equal(isValidPlate("1223AEI"), false, "1223 AEI is an invalid plate");
    assert.equal(isValidPlate("1222BCDB"), false, "1222 BCDB is an invalid plate");
    assert.equal(isValidPlate("2222BC"), false, "2222 BC is an invalid plate");
});

//The one that has 4 letters gives us as valid but is invalid.

test("Not Valid Plates (wrong order)",function(assert){
    assert.equal(isValidPlate("BCD1234"), false, "BCD1234 is an invalid plate");
});



